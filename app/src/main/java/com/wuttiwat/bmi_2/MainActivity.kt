package com.wuttiwat.bmi_2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import java.text.NumberFormat
import com.wuttiwat.bmi_2.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        fun displayBmi() {
            binding.bmiResult.text = "0"
            binding.bmiText.text = ""
        }

        displayBmi()
        binding.calculateButton.setOnClickListener { calculateBmi() }
        binding.heavyBodyEditText.setOnKeyListener { view, keyCode, _ -> handleKeyEvent(view, keyCode)
        }
        binding.heightBodyEditText.setOnKeyListener { view, keyCode, _ -> handleKeyEvent(view, keyCode)
        }
    }

    private fun displayBmi(){
        binding.bmiResult.text = "0"
        binding.bmiText.text = ""
    }


    private fun calculateBmi() {
        val stringInTextField = binding.heavyBodyEditText.text.toString()
        val heavy = stringInTextField.toDoubleOrNull()
        val stringInTextField1 = binding.heightBodyEditText.text.toString()
        val height = stringInTextField1.toDoubleOrNull()
        if (heavy == null || height == null) {
            displayBmi()
            return
        }
        else{
            val height1 = height/100
            var bmi = heavy/(height1*height1)
            val roundedNumber = "%.2f".format(bmi)
            binding.bmiResult.text = roundedNumber.toString()
            bmi = kotlin.math.ceil(bmi)

            when{
                bmi < 18.50 -> binding.bmiText.text = "under"
                bmi in 18.50..22.90 -> binding.bmiText.text = "nomal"
                bmi in 23.00..24.90 -> binding.bmiText.text = "risk"
                bmi in 25.00..29.90 -> binding.bmiText.text = "over"
                else -> binding.bmiText.text = "obese"
            }
        }

    }
    private fun handleKeyEvent(view: View, keyCode: Int): Boolean {
        if (keyCode == KeyEvent.KEYCODE_ENTER) {
            val inputMethodManager = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
            return true
        }
        return false
    }

}